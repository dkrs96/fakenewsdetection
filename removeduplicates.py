import pandas as pd


# Löschen von Dopplungen
df = pd.read_csv('news2cleaned.csv', encoding='utf-8')
print(df.__len__())
df.dropna(how='any', inplace=True, ignore_index=True)
print(df.__len__())
df.drop_duplicates(subset=['title', 'text', 'subject'], inplace=True, ignore_index=True)
# if its the same artikel but with diiffert subjects it gets caught here
df_dubcatcher = df[df.duplicated(subset=['title', 'text'], keep=False)]
# test if headlines are the same and save direktion of headine
i = 0

for i, sub in enumerate(df_dubcatcher['title']):
    for j, checkifdub in enumerate(df['title']):
        if checkifdub == sub:
            df.at[i, 'subject'] = df.at[i, 'subject'] + ", " + df.at[j, 'subject']
            #print(i, j)
            #print(df.at[i, 'subject'])
df.drop_duplicates(subset=['title', 'text'], inplace=True, ignore_index=True)
print(df.__len__())
df.to_csv('news2dropdub.csv', encoding='utf-8')



df = pd.DataFrame({
    'brand': ['Yum Yum Yum Yum', 'Yum Yum', 'Indomie', 'Indomie', 'Indomie'],
     'style': ['cup', 'cup', 'cup', 'pack', 'pack'],
     'rating': [4, 4, 3.5, 15, 5]
})
df.drop_duplicates(subset=['brand'], inplace=True)

print(df)

