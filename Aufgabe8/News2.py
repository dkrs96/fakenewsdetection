import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline

# Eine Ausführlichere Erklärung gibt es in der Doku
# Der importierte Datensatz befindet sich im Ordner
# 30GB RAM werden benötigt. Wenn nicht vorhanden max_features runtersetzen

# Importieren des News2 Datensatz
data = pd.read_csv('News2.csv', encoding='latin1')

# Fehlende Werte werden mit 0 befüllt (falls diese vorhanden sind)
data.fillna(0, inplace=True)

# Aufteilung in Trainings- und Testdaten 80%Test und 20%Training
# Mit random_state=42 stellen wir sicher, dass die Zufallskomponten reproduzierbar sind. (Mehr dazu in der Doku)
train_data, test_data = train_test_split(data, test_size=0.2, random_state=42)

# Filtert Spalte text und title so, dass nur noch Zeilen mit einem Wert vom Typ String bleiben
train_data = train_data[train_data['title'].apply(lambda x: isinstance(x, str))]
train_data = train_data[train_data['text'].apply(lambda x: isinstance(x, str))]
test_data = test_data[test_data['title'].apply(lambda x: isinstance(x, str))]
test_data = test_data[test_data['text'].apply(lambda x: isinstance(x, str))]

# Verwendung von N-Grammen
# max_features müssen eventuell angepasst werden, falls weniger als 30GB RAM vorhanden sind
vectorizer_title = CountVectorizer(ngram_range=(1, 2), max_features=35000)
vectorizer_text = CountVectorizer(ngram_range=(1, 2), max_features=35000)

# Umwandeln in einen Vektor der die Häufigkeit der Wörter / Wortpaare beinhaltet
X_train_ngrams_title = vectorizer_title.fit_transform(train_data['title'])
X_test_ngrams_title = vectorizer_title.transform(test_data['title'])
X_train_ngrams_text = vectorizer_text.fit_transform(train_data['text'])
X_test_ngrams_text = vectorizer_text.transform(test_data['text'])


# Zusammenführen des aufgeteilten Datensatzes
preprocessor = ColumnTransformer(
    transformers=[
        ('text_title', vectorizer_title, 'title'),
        ('text_text', vectorizer_text, 'text')
    ], sparse_threshold=0)

# Erstellt eine Pipeline die die angepassten Daten und ein RandomForestClassifier beinhaltet
model = Pipeline([
    ('preprocessor', preprocessor),
    ('classifier', RandomForestClassifier(random_state=42))
])

# Aufteilung in X und y Variablen
# X beinhaltet alle Spalten außer fake
# y (Zielspalte) beinhaltet nur fake
X_train = train_data.drop(['fake'], axis=1)
y_train = train_data['fake']
X_test = test_data.drop(['fake'], axis=1)
y_test = test_data['fake']

# Trainiert das Modell
model.fit(X_train, y_train)

# Evaluiert das Modell auf den Testdaten und berechnet die Genauigkeit
accuracy = model.score(X_test, y_test)
print("Accuracy:", accuracy)

# Accuracy: 0.9530685920577617