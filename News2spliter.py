# Hier Nehmen wir die News2 Cleaned und lassen uns ein balances samples ausgeben, indem ca die gleiche Menge von True und False Daten enhalten sind
import pandas as pd
df = pd.read_csv('news_2_cleaned.csv',sep=';',  encoding='utf-8')

true_samples = df[df['fake'] ==  0]
false_samples = df[df['fake'] == 1]

num_samples = min(len(true_samples), len(false_samples))
# Random_state 42 ist dafür da damit ein Test repilzierbar ist ähnlich wie ein Seed
balanced_samples = pd.concat([true_samples.sample(n=num_samples, random_state=42),
                              false_samples.sample(n=num_samples, random_state=42)])

balanced_samples = balanced_samples.sample(frac=1, random_state=42).reset_index(drop=True)
print("Nun ist es durchgelaufen ")

balanced_samples.to_csv('0.balancednews2.csv', index=False, sep=';', encoding='utf-8')
print("Nun ist gespeichert")
