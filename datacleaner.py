import re
import xml.etree.ElementTree as ET
import spacy
import nltk
from nltk import ngrams
from spacy.tokens.doc import Counter
from collections import Counter
import pandas as pd
from tqdm import tqdm

nlp = spacy.load('de_core_news_lg')

c_all_fake = Counter()
c_all_true = Counter()
c_all = Counter()

all_articels = []
df = pd.read_csv('news2/fake_news_processed.csv', sep=';', encoding='utf-8')
#df = df[:1]
toprint = False
print(df.__len__())
# Ersetzt alle : mit .
df['text'].replace(':', '.', regex=True, inplace=True)
# Entfernt alle Zeilen die einen Nan Wert besitzen
df.dropna(how='any', inplace=True, ignore_index=True)
print(df.__len__())
# Schaut in title und in text nach Dopplungen und löscht dann einen davon
df.drop_duplicates(subset=['title', 'text'], inplace=True, ignore_index=True)
print(df.__len__())
#lemmatizer = nlp.add_pipe("lemmatizer", config={'mode': 'rule', 'overwrite': True})
#später testen welche daten falsch erkannt werden
#df['lemmatized_text'] = df['text'].apply(lambda text: ' '.join([token.lemma_ for token in nlp(text)]))

#df.to_csv('News1finalCleanTrainingData_Lemmatized.csv', index=False, encoding='utf-8')

i = 0
coun = df.size/10
cleaned_text = []
# Tokenized text um diese einzeln zu säubern
with tqdm(total=coun) as pbar:
    for text in df['text']:
        text = ' '.join([token.lemma_ for token in nlp(text)])
        pbar.update(1)
        cleaned_texte = ""
        text_tok = nlp(text)
        for sent in text_tok.sents:
            sent = re.sub(r'\?!\.', ' ', sent.text)
            sent = re.sub(r'[:,–\-;"\)\(\/\xa0\n0-9]', ' ', sent)
            sent = re.sub(r'-pron-', '', sent)
            sent_tok = nlp(sent)
            gefilterte_tokens = []
            bearbeiteter_text = []
            for token in sent_tok:
                # Schmeißt alle raus die Anforderungen nicht erfüllen
                if not token.is_punct and not token.is_digit and not token.is_space and not token.is_stop:
                    # Löscht alle Zeilenumbrüche
                    gewünschte_token = token.text.strip().replace('\n', '')
                    # Entfernt alle Leerzeichen
                    gewünschte_token = ' '.join(gewünschte_token.split())
                    # Falls Token es bis hierhin geschafft hat, wird er als lowercase in die Liste hinzugefügt
                    if gewünschte_token:
                        gefilterte_tokens.append("".join(gewünschte_token.lower()))
                        #print(token, token.is_stop)
            cleaned_texte = cleaned_texte +  (" ").join(gefilterte_tokens) + "|||"
        cleaned_text.append((cleaned_texte))
df['text'] = cleaned_text
cleaned_title = []

#Tokenized Title um diese jeweils einzeln zu cleanen
with tqdm(total=coun) as pbar:
    for text in df['title']:
        text = ' '.join([token.lemma_ for token in nlp(text)])
        pbar.update(1)
        cleaned_texte = ""
        text_tok = nlp(text)
        sentences = [sent.text for sent in text_tok.sents]
        for sents in sentences:
            sents = re.sub(r'\?!\.', ' ', sents)
            sents = re.sub(r'[:,–\-\;\"\)\(\/\xa0\n0-9]', ' ', sents)
            sent_tok = nlp(sents)
            gefilterte_tokens = []
            bearbeiteter_text = []
            for token in sent_tok:
                # Schmeißt alle raus die Anforderungen nicht erfüllen
                if not token.is_punct and not token.is_digit and not token.is_space and not token.is_stop:
                    # Löscht alle Zeilenumbrüche
                    gewünschte_token = token.text.strip().replace('\n', '')
                    # Entfernt alle Leerzeichen
                    gewünschte_token = ' '.join(gewünschte_token.split())
                    # Falls Token es bis hierhin geschafft hat, wird er als lowercase in die Liste hinzugefügt
                    if gewünschte_token:
                        gefilterte_tokens.append("".join(gewünschte_token.lower()))
                        #print(gefilterte_tokens)
                #.append(gefilterte_tokens)
            cleaned_texte = cleaned_texte + (" ").join(gefilterte_tokens)
        cleaned_title.append((cleaned_texte))
df['title'] = cleaned_title

df.to_csv('news_2_cleaned.csv', index=False, sep=';', encoding='utf-8')

true_news = df.loc[df['type'] == 'True']
for text in true_news['text']:
    # print(text)
    doc = nlp(text)
    c_all_true = c_all_true + Counter([token.text for token in doc])
    # tqdm(range(true_news.__len__), )
fake = df.loc[df['type'] == 'False']
for text in fake['text']:
    # print(text)
    doc = nlp(text)
    c_all_fake = c_all_fake + Counter([token.text for token in doc])
    # tqdm(range(true_news.__len__), )
c_all = c_all_fake + c_all_true
# Geb von allen / den wahren / den falschen die 100 häufigsten Wörter an
print(c_all.most_common(100))
print(c_all_true.most_common(100))
print(c_all_fake.most_common(100))
with open('c_all', 'w') as w:
    w.write("The word frequency is " + str(c_all))
with open('c_all_fake', 'w') as w:
    w.write("The word frequency is " + str(c_all_fake))
with open('c_all_true_test1', 'w') as w:
    w.write("The word frequency is " + str(c_all_true))


# Ergebnis zur Überprüfung [('|||', 177154), ('-pron-', 46769), ('trump', 27186), ('.', 22069), ('s', 21805), ('u.s', 19174), ('president', 14149), ('reuters', 13977), ('state', 12829), ('“', 11234), ('year', 11048), ('government', 9778), ('i', 8570), ('house', 8481), ('new', 8354), ('republican', 8271), ('people', 7611), ('tell', 7565), ('party', 7527), ('united', 7497), ('election', 7399), ('-', 7286), ('official', 7127), ('country', 6769), ('washington', 6275), ('states', 5972), ('include', 5939), ('campaign', 5845), ('group', 5561), ('vote', 5411), ('court', 5346), ('week', 5272), ('leader', 5265), ('donald', 5184), ('it', 5183), ('security', 5002), ('north', 4962), ('he', 4921), ('law', 4835), ('we', 4790)]

