import pandas as pd

true_news = pd.read_csv('csv/True.csv')
fake_news = pd.read_csv('csv/Fake.csv')

link_filter = fake_news['date'].str.contains('http|www')
fake_news = fake_news[~link_filter]

youtube_link_pattern = r'https?://(www\.)?youtube\.[a-zA-Z]{2,}/'
youtube_watch_pattern = r'watch\?v=[^&\s]+'

youtube_link_filter = fake_news['text'].str.contains(youtube_link_pattern, regex=True)

# Entfernt die Youtube Links nun in der Spalte
fake_news['text'] = fake_news['text'].replace(to_replace=youtube_link_pattern, value='', regex=True)
fake_news['text'] = fake_news['text'].replace(to_replace=youtube_watch_pattern, value='', regex=True)

fake_news.to_csv('cleaned_fake_news.csv', index=False)
