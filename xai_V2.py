import spacy
import pandas as pd

nlp = spacy.load('en_core_web_sm')

df = pd.read_csv('0news1biastest.csv', encoding='utf-8')
df = df[:1]

all_articles_tokens = []

for text in df['text']:
    filtered_tokens = []
    doc = nlp(text)
    for token in doc:
        if not token.is_punct and not token.is_digit and not token.is_space and not token.is_stop:
            filtered_tokens.append(token.lemma_.lower())  # Using lemmatization for consistency

    all_articles_tokens.append(filtered_tokens)

# Function to generate combinations for each part of the text
def generate_combinations_for_part(part_tokens):
    combinations_list = []
    for i in range(len(part_tokens)):
        combinations_list.append(part_tokens[i])
    for i in range(len(part_tokens) - 1):
        combination = " ".join([part_tokens[i], part_tokens[i + 1]])
        combinations_list.append(combination)
    return combinations_list

# Create a new DataFrame to store combinations
new_df_rows = []

# Split the text into parts and generate combinations for each part
num_parts = 3  # You can adjust the number of parts
tokens_per_part = len(all_articles_tokens[0]) // num_parts

for i in range(num_parts):
    start_index = i * tokens_per_part
    end_index = (i + 1) * tokens_per_part if i < num_parts - 1 else len(all_articles_tokens[0])

    part_tokens = all_articles_tokens[0][start_index:end_index]
    combinations_list = generate_combinations_for_part(part_tokens)

    for combination_text in combinations_list:
        new_row = df.iloc[0].copy()
        new_row['text'] = combination_text
        new_df_rows.append(new_row)

# Create a new DataFrame with the combinations
new_df = pd.DataFrame(new_df_rows)

new_df.to_csv('0.xai_V2news2.csv', index=False, encoding='utf-8')
# Print or save the new DataFrame as needed
print(new_df)
