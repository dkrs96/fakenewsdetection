import re
import xml.etree.ElementTree as ET
import spacy
import nltk
from nltk import ngrams
from spacy.tokens.doc import Counter
from collections import Counter

nlp = spacy.load('de_core_news_sm')

paths = [
    'AuswärtigesAmt.xml'  # , 'Bundesregierung.xml', 'Bundestagspräsidenten.xml', 'Bundespräsidenten.xml'
]
allspeeches = []

for speech in paths:
    tree = ET.parse(speech)
    root = tree.getroot()
    dlist = []
    for text in root:
        for rawtext in text:
            dlist.append(rawtext.text)
    allspeeches.extend(dlist)

# print(allspeeches)

chunk_size = 1000000
# erzeugt einzelnen string
allspeeches = " ".join(allspeeches)

# allspeeches = allspeeches.split()
# begrenzt auf maximal eine Mio. Zeichen
allspeeches = allspeeches[:chunk_size]
# print(allspeeches)
doc = nlp(allspeeches)
# https://stackoverflow.com/questions/37253326/how-to-find-the-most-common-words-using-spacy
# gets freq from counter
"""
4a

Most common unfiltered words
word_freq = Counter([   token.text for token in doc

])
common_words = word_freq.most_common(10)
print(common_words)
#[(',', 8344), ('.', 7395), ('und', 4647), ('die', 4289), ('der', 3977), ('in', 2898), ('\n', 2735), ('wir', 1616), ('zu', 1605), ('ist', 1561)]

Least common unfiltered words

#regex

word_freq = Counter([token.text for token in doc

])
#https://docs.python.org/3/library/collections.html#collections.Counter
common_words = word_freq.most_common()[:-10-1:-1]
print(common_words)
[('Einfl', 1), ('zusammenbrach', 1), ('Schreckens', 1), ('Gleichgewichts', 1), ('zynische', 1), ('Lebensraumpolitik', 1), ('nationalsozialistischer', 1), ('auszudehnen', 1), ('fixen', 1), ('besessen', 1)]
"""

# Wie kann dieses Wissen beim S¨aubern des Korpus helfen?
#

# allspeeches = re.sub(r'[\?\!\.\:\,\–\-\;\"\)\(\/\xa0\n0-9]', ' ', allspeeches)
allspeeches = re.sub(r'[\?\!\.\:\,\–\-\;\"\)\(\/\xa0\n0-9]', ' ', allspeeches)

doc = nlp(allspeeches)
"""
usefulltoken = []
for token in doc:
 if not token.is_stop and not token.is_space:
    stoken =  "".join(token.text)
    usefulltoken.append(stoken.lower())

ergebnis 4
[(('damen', 'herren'), 133), (('vereinten', 'nationen'), 91), (('europäischen', 'union'), 57), (('europäische', 'union'), 42),
 (('kolleginnen', 'kollegen'), 31), (('auswärtigen', 'amt'), 30), (('bürgerinnen', 'bürger'), 28), (('frieden', 'sicherheit'), 25), (('auswärtige', 'amt'), 23), (('herr', 'präsident'), 22)]
"""

gefilterte_tokens = []
"""
for token in doc:
    # Schmeißt alle raus die Anforderungen nicht erfüllen
    if not token.is_punct and not token.is_digit and not token.is_space and not token.is_stop:
        # Löscht alle Zeilenumbrüche
        gewünschte_token = token.text.strip().replace('\n', '')
        # Entfernt alle Leerzeichen
        gewünschte_token = ' '.join(gewünschte_token.split())
        # Falls Token es bis hierhin geschafft hat, wird er als lowercase in die Liste hinzugefügt
        if gewünschte_token:
            gefilterte_tokens.append(gewünschte_token.lower())

bearbeiteter_text = ' '.join(gefilterte_tokens)
"""
# print(bearbeiteter_text)

# for token in doc:
#   # Schmeißt alle raus die Anforderungen nicht erfüllen
#  if token.is_punct


sätze = []
# document = nlp(doc)
sentences = [sent.text for sent in doc.sents]

for sentence in sentences:
    print(sentence)
    sätze.append(sentence)
print(sätze)

#4a

#most common filtered words
word_freq = Counter(usefulltoken)
common_words = word_freq.most_common(10)
print(common_words)
#print(len(usefulltoken))
#[('europa', 463), ('deutschland', 361), ('menschen', 285), ('europäischen', 238), ('deutschen', 225), ('welt', 219), ('gemeinsam', 189), ('eu', 182), ('europäische', 162), ('zusammenarbeit', 161)]


#least common filtered words
common_words = word_freq.most_common()[:-10-1:-1]
print(common_words)
#Ergebnisse -> [('Einfl', 1), ('zusammenbrach', 1), ('Schreckens', 1), ('Gleichgewichts', 1), ('zynische', 1), ('Lebensraumpolitik', 1), ('nationalsozialistischer', 1), ('auszudehnen', 1), ('fixen', 1), ('besessen', 1)]


# ngrams Aufgabe 4b
# for



n = 2
allgrams = ngrams(bearbeiteter_text.split(), n)
sorted(ngrams_counts)

ngram_freq = Counter(allgrams)
mostused_ngrams = ngram_freq.most_common(10)
print("Most used ngrams " + mostused_ngrams)

text_biggrams = [ngrams(sent) for sent in usefulltoken]

print(text_biggrams)

print(usefulltoken)


print(usefulltoken)
allspeeches = re.sub(r'[\?\!\.\:\,\–\-\xa0\n]', ' ', allspeeches)
allspeeches = allspeeches.split()

print(allspeeches)


