import pdb
import re
import xml.etree.ElementTree as ET
import spacy
import nltk
from nltk import ngrams
from spacy.tokens.doc import Counter
from collections import Counter
import pandas as pd
from tqdm import tqdm

nlp = spacy.load('en_core_web_sm')
c_all_fake = Counter()
c_all_true = Counter()
c_all = Counter()

all_articels = []
bo = False
if bo:
    df = pd.read_csv('News1Train9.01', encoding='utf-8')
else:
    df = pd.read_csv('0_News1TestData.csv', encoding='utf-8')
#Manuelles Entfernen von Wörtern die einem Bias hervorrufen
df['text'].replace('-pron-', '', regex=True, inplace=True)
df['text'].replace('trump', '', regex=True, inplace=True)
df['text'].replace('reuters', '', regex=True,inplace=True)
df['text'].replace('reuter', '', regex=True,inplace=True)
df['text'].replace('donald', '', regex=True, inplace=True)
df['text'].replace('clinton', '', regex=True, inplace=True)
df['text'].replace('hillary', '', regex=True, inplace=True)
df['text'].replace('reut', '', regex=True, inplace=True)
df['text'].replace('image', '', regex=True, inplace=True)
df['text'].replace(' t ', '', regex=True,inplace=True)
df['text'].replace(' s ', '', regex=True,inplace=True)

df['text'].replace('hillary', '', regex=True, inplace=True)
df['text'].replace(r'[\?\!\.\:\,\–\-\;\"\)\(\/\xa0\n0-9]', ' ', regex=True, inplace=True)
df['title'].replace('-pron-', '', regex=True, inplace=True)
df['title'].replace('trump', '', regex=True, inplace=True)
df['title'].replace('reuters', '', regex=True, inplace=True)
df['title'].replace('reuter', '', regex=True, inplace=True)
df['title'].replace('donald', '', regex=True, inplace=True)
df['title'].replace('hillary', '', regex=True, inplace=True)
df['text'].replace(' rs', '', regex=True, inplace=True)
df['title'].replace('clinton', '', regex=True, inplace=True)
#df['title'].replace('reut rs', '', regex=True,inplace=True)
df['title'].replace('image', '1', regex=True, inplace=True)

df['title'].replace(r'[\?\!\.\:\,\–\-\;\"\)\(\/\xa0\n0-9]', ' ', regex=True, inplace=True)
coun = df.size/5
cleaned_text = []
"""
with tqdm(total=coun) as pbar:
    "for text in df['text']:
        text = nlp(text)
        cleaned_texte = ""
        pbar.update(1)
        for tokenn in text:
            sent = re.sub('trump' , '', tokenn.text)
            sent = re.sub(r'[:,–\-;"\)\(\/\xa0\n0-9]', ' ', sent)
            sent = re.sub('-pron-', '', sent)
            #sent_tok = nlp(sent)
            gefilterte_tokens = []
            bearbeiteter_text = []
            for token in sent_tok:
                # Schmeißt alle raus die Anforderungen nicht erfüllen
                if not token.is_punct and not token.is_digit and not token.is_space and not token.is_stop:
                    # Löscht alle Zeilenumbrüche
                    gewünschte_token = token.text.strip().replace('\n', '')
                    # Entfernt alle Leerzeichen
                    gewünschte_token = ' '.join(gewünschte_token.split())
                    # Falls Token es bis hierhin geschafft hat, wird er als lowercase in die Liste hinzugefügt
                    if gewünschte_token:
                        gefilterte_tokens.append("".join(gewünschte_token.lower()))
                        # print(token, token.is_stop)

            cleaned_texte = cleaned_texte + " " + ("").join(sent)
        cleaned_text.append((cleaned_texte))
    pbar.close()
df['text'] = cleaned_text
pbar.close()"""
#for col in df
if bo:
    df.to_csv('0news1biascl.csv', index=False, encoding='utf-8')
else:
    df.to_csv('0news1biastest.csv', index=False, encoding='utf-8')
#[('|||', 177154), ('-pron-', 46769), ('trump', 27186), ('.', 22069), ('s', 21805), ('u.s', 19174), ('president', 14149), ('reuters', 13977), ('state', 12829), ('“', 11234), ('year', 11048), ('government', 9778), ('i', 8570), ('house', 8481), ('new', 8354), ('republican', 8271), ('people', 7611), ('tell', 7565), ('party', 7527), ('united', 7497), ('election', 7399), ('-', 7286), ('official', 7127), ('country', 6769), ('washington', 6275), ('states', 5972), ('include', 5939), ('campaign', 5845), ('group', 5561), ('vote', 5411), ('court', 5346), ('week', 5272), ('leader', 5265), ('donald', 5184), ('it', 5183), ('security', 5002), ('north', 4962), ('he', 4921), ('law', 4835), ('we', 4790)]
