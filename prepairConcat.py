import pandas as pd

dataset_true_news = pd.read_csv('csv/True.csv', encoding='utf-8')
dataset_fake_news = pd.read_csv('csv/Fake.csv', encoding='utf-8')
#labelcolum eingesetzt
dataset_fake_news.insert(4, "type", False)
dataset_true_news.insert(4, "type", True)
#datensatze werden zusammengefasst
df_concat = pd.concat([dataset_fake_news, dataset_true_news], ignore_index=True)

df_concat['date'].replace(' ', '.', regex=True, inplace=True)
df_concat['date'].replace(',', '', regex=True, inplace=True)
df_concat['date'].replace(r'\.$', '', regex=True, inplace=True)
df_concat['date'] = pd.to_datetime(df_concat['date'], format='.%Y', errors='ignore')

df_concat.dropna(how='any', inplace=True)
print(df_concat.__len__())
#Löscht alle Doppelten mit dem selben Thema
df_concat.drop_duplicates(subset=['title', 'text', 'subject'], inplace=True, ignore_index=True)
#if its the same artikel but with diiffert subjects it gets caught here
df_dubcatcher = df_concat[df_concat.duplicated(subset=['title', 'text'], keep=False)]
#test if headlines are the same and save direktion of headine
i = 0
# TODO kommentieren was das hier macht
for i, sub in enumerate(df_dubcatcher['title']):
    for j, checkifdub in enumerate(df_concat['title']):
        if checkifdub == sub:
            df_concat.at[i, 'subject'] = df_concat.at[i, 'subject'] + ", " + df_concat.at[j, 'subject']
            print(df_concat.at[i, 'subject'])


print(df_concat.__len__())
df_concat.to_csv('News1combined.csv', encoding='utf-8')


