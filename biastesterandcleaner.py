import re
import xml.etree.ElementTree as ET
import spacy
import nltk
from nltk import ngrams
from spacy.tokens.doc import Counter
from collections import Counter
import pandas as pd
from tqdm import tqdm

nlp = spacy.load('en_core_web_sm')
c_all_fake = Counter()
c_all_true = Counter()
c_all = Counter()

all_articels = []
to_bias = pd.read_csv('0_News1TestData.csv', encoding='utf-8')



true_news = to_bias.loc[to_bias['type'] == 'True']
for text in true_news['text']:
    #print(text)
    doc = nlp(text)
    c_all_true = c_all_true + Counter([token.text for token in doc])
    #tqdm(range(true_news.__len__), )
fake = to_bias.loc[to_bias['type'] == 'False']
for text in fake['text']:
    #print(text)
    doc = nlp(text)
    c_all_fake = c_all_fake + Counter([token.text for token in doc])
    #tqdm(range(true_news.__len__), )
c_all = c_all_fake+c_all_true
print(c_all.most_common(100))
print(c_all_true.most_common(100))
print(c_all_fake.most_common(100))
with open('c_all_news1', 'w') as w:
    w.write("The word frequency is " + str(c_all))
with open('c_all_fake_news1', 'w') as w:
    w.write("The word frequency is " + str(c_all_fake))
with open('c_all_true_news1', 'w') as w:
    w.write("The word frequency is " + str(c_all_true))
#[('|||', 177154), ('-pron-', 46769), ('trump', 27186), ('.', 22069), ('s', 21805), ('u.s', 19174), ('president', 14149), ('reuters', 13977), ('state', 12829), ('“', 11234), ('year', 11048), ('government', 9778), ('i', 8570), ('house', 8481), ('new', 8354), ('republican', 8271), ('people', 7611), ('tell', 7565), ('party', 7527), ('united', 7497), ('election', 7399), ('-', 7286), ('official', 7127), ('country', 6769), ('washington', 6275), ('states', 5972), ('include', 5939), ('campaign', 5845), ('group', 5561), ('vote', 5411), ('court', 5346), ('week', 5272), ('leader', 5265), ('donald', 5184), ('it', 5183), ('security', 5002), ('north', 4962), ('he', 4921), ('law', 4835), ('we', 4790)]
