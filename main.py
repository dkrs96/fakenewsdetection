import spacy
import xml.etree.ElementTree as ET
import pandas as pd
import numpy
import csv
import datetime


dataset_true_news = pd.read_csv('csv/True.csv', encoding='utf-8')
dataset_fake_news = pd.read_csv('csv/Fake.csv', encoding='utf-8')

dataset_fake_news = dataset_fake_news[~dataset_fake_news['title'].str.contains(r'\[Video\]', case=False)]
dataset_fake_news = dataset_fake_news[dataset_fake_news['text'] != r' ' or dataset_fake_news['text'] != '  ']
dataset_fake_news.insert(4, "type", False)
dataset_true_news.insert(4, "type", True)
#Date Format
dataset_fake_news = dataset_fake_news[~dataset_fake_news['date'].str.contains(r'\[Video\]', case=False)]
df = pd.DataFrame(dataset_fake_news['date'])
df = pd.DataFrame(dataset_fake_news['date'])
dff = pd.DataFrame(dataset_true_news['date'])
dataset_true_news['date'].replace(' ', '.', regex=True, inplace=True)
dataset_true_news['date'].replace(',', '', regex=True, inplace=True)
dataset_fake_news['date'].replace(' ', '.', regex=True, inplace=True)
dataset_fake_news['date'].replace(',', '', regex=True, inplace=True)
dataset_true_news['date'].replace(r'\.$', '', regex=True, inplace=True)

df.replace('-[1]', '.', regex=True, inplace=True)
# In Date werden alle Monate vereinheitlicht, da manche Monate wie zB Januar in mehreren geschriebenen Formen da stehen
dataset_fake_news['date'].replace('January', '1', regex=True, inplace=True)
dataset_fake_news['date'].replace('Jan', '1', regex=True, inplace=True)
dataset_fake_news['date'].replace('February', '2', regex=True, inplace=True)
dataset_fake_news['date'].replace('Feb', '2', regex=True, inplace=True)
dataset_fake_news['date'].replace('March', '3', regex=True, inplace=True)
dataset_fake_news['date'].replace('Mar', '3', regex=True, inplace=True)
dataset_fake_news['date'].replace('April', '4', regex=True, inplace=True)
dataset_fake_news['date'].replace('Apr', '4', regex=True, inplace=True)
dataset_fake_news['date'].replace('May', '5', regex=True, inplace=True)
dataset_fake_news['date'].replace('June', '6', regex=True, inplace=True)
dataset_fake_news['date'].replace('Jun', '6', regex=True, inplace=True)
dataset_fake_news['date'].replace('July', '7', regex=True, inplace=True)
dataset_fake_news['date'].replace('Jul', '7', regex=True, inplace=True)
dataset_fake_news['date'].replace('August', '8', regex=True, inplace=True)
dataset_fake_news['date'].replace('Aug', '8', regex=True, inplace=True)
dataset_fake_news['date'].replace('September', '9', regex=True, inplace=True)
dataset_fake_news['date'].replace('Sep', '9', regex=True, inplace=True)
dataset_fake_news['date'].replace('October', '10', regex=True, inplace=True)
dataset_fake_news['date'].replace('Oct', '10', regex=True, inplace=True)
dataset_fake_news['date'].replace('November', '11', regex=True, inplace=True)
dataset_fake_news['date'].replace('Nov', '11', regex=True, inplace=True)
dataset_fake_news['date'].replace('December', '12', regex=True, inplace=True)
dataset_fake_news['date'].replace('Dec', '12', regex=True, inplace=True)



#df = pd.to_datetime(df, infer_datetime_format=True)
#df = pd.to_datetime(df, format='%d-%m-%y', errors='coerce')
# Hier wird versucht das ganze in ein bestimmtes Datum Format zu formatieren
df = pd.to_datetime(dataset_fake_news['date'], format='.%Y', errors='ignore')



# Mit diesem Befehl werden jegliche Dopplungen gelöscht
fakeNewspath.drop_duplicates()
# Speichern der bisher bearbeiteten Daten im UTF8 Format
dataset_fake_news.to_csv('cleaned_fake_news.csv', encoding='utf-8')
dataset_true_news.to_csv('cleaned_true_news.csv', encoding='utf-8')
